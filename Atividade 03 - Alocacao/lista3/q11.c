// Considere a estrutura de dados Hidrocarboneto formada pelos campos C e H
// ambos inteiros correspondentes à quantidade de carbonos e hidrogênios de
// determinado composto. Crie uma função que, após alocar dinamicamente uma
// variável do tipo Hidrocarboneto, receba do usuário a quantidade de carbonos e
// hidrogênios possuída por determinado elemento e imprima a massa molecular
// do composto. Considere que C (carbono) = 12 e H (hidrogênio) = 1.

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int * criarVetor(int n){
    int * pont;
    pont = (int *) calloc(n, sizeof(int));
    return pont;
}

void liberarVetor(int *vetor){
    free(vetor);
}

int main(){
    int n = 2;
    int * vetor = criarVetor(n);
    scanf("%d  %d", &vetor[0], &vetor[1]);
    printf("%d\n", vetor[0]*12 + vetor[1]);
    liberarVetor(vetor);
}