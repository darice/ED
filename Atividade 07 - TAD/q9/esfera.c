#include<stdio.h>
#include<stdlib.h>
#include "esfera.h"

struct esfera{
    float raio;
};

Esfera *criar(float raio){
     Esfera *esfera;
    esfera = (Esfera*) calloc(1, sizeof(Esfera));
    esfera->raio=raio;
}

void area(Esfera*esfera){
    float area = 4*3.14*esfera->raio*esfera->raio;
    printf("Area: %f\n", area);
}

void volume(Esfera*esfera){
    float volume = (4*3.14*esfera->raio*esfera->raio*esfera->raio)/3.0;
    printf("Volume: %f\n", volume);
}