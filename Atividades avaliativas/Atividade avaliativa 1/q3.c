#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

typedef struct ponto
{
    int x, y;
} Ponto;

typedef struct pessoa
{
    char nome[100];
    Ponto p;
} Pessoa;

float calcDistancia(int x1, int y1, int x2, int y2)
{
    return (float)sqrt(pow(x2 - x1, 2.0) + pow(y2 - y1, 2.0));
}

void main()
{
    srand(time(NULL));
    Pessoa *pessoa;
    Ponto *centroMassa;

    int n;
    printf("digite o tamanho de n: ");
    scanf("%d", &n);
    pessoa = (Pessoa *)calloc(n, sizeof(Pessoa));
    for (int i = 0; i < n; i++)
    {
        printf("digite o nome da pessoa: ");
        scanf("%s", pessoa[i].nome);
        pessoa[i].p.x = rand() % 50;
        pessoa[i].p.y = rand() % 50;
    }

    centroMassa = (Ponto *)calloc(1, sizeof(Ponto));
    int somaX = 0, somaY = 0;
    for (int i = 0; i < n; i++)
    {
        somaX += pessoa[i].p.x;
        somaY += pessoa[i].p.y;
    }
    somaX /= n;
    somaY /= n;
    centroMassa[0].x = somaX;
    centroMassa[0].y = somaY;

    int maiorX = pessoa[0].p.x;
    int menorX = pessoa[0].p.x;
    int maiorY = pessoa[0].p.y;
    int menorY = pessoa[0].p.y;

    for (int i = 0; i < n; i++)
    {
        if (pessoa[i].p.x > maiorX)
        {
            maiorX = pessoa[i].p.x;
        }
        if (pessoa[i].p.x < menorX)
        {
            menorX = pessoa[i].p.x;
        }
        if (pessoa[i].p.x > maiorY)
        {
            maiorY = pessoa[i].p.y;
        }
        if (pessoa[i].p.x < menorY)
        {
            menorY = pessoa[i].p.y;
        }
    }
    int tX = maiorX - menorX;
    int tY = maiorY - menorY;
    int diametro;

    if (tX > tY)
    {
        diametro = tY;
    }
    else
    {
        diametro = tX;
    }
    float raio =(float) diametro/2;

    for (int i = 0; i < n; i++)
    {
        float distancia = calcDistancia(centroMassa[0].x, centroMassa[0].y, pessoa[i].p.x, pessoa[i].p.x);
        if(distancia < (diametro/2.0)){
            printf("\nPessoa: %s\n", pessoa[i].nome);
            printf("ponto (%d,%d)\n", pessoa[i].p.x, pessoa[i].p.x );

        }
    }

    free(pessoa);
    free(centroMassa);

}