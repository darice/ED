// • 2 – Desenvolva um algoritmo que crie uma função que receba um vetor e
// inverta os seus valores. O vetor invertido deve ser acessado no método main
// para impressão dos valores antes e depois

#include <stdio.h>

void func(int *vetor1, int *vetor2)
{
    for (int i = 0; i < 3; i++)
    {
        vetor2[i] = vetor1[2 - i];
    }
}

void main()
{
    int vetor1[3] = {1, 2, 3};
    int vetor2[3];
    printf("antes: ");
    for (int i = 0; i < 3; i++)
    {
        printf("%d, ", vetor1[i]);
    }
    func(vetor1, vetor2);
    printf("\ndepois: ");
    for (int i = 0; i < 3; i++)
    {
        printf("%d, ", vetor2[i]);
    }
}