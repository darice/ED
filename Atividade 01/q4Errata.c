// Dada uma matriz de M x N com valores randômicos preenchidos aleatoriamente no intervalo de 1 a
// 250. Cria uma solução que apresente todos números que se repetem e a quantidade de vezes que cada
// número se repetiu. Além disso, mostrar também quais números que poderiam ter ocorrências, mas não
// tiveram.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main()
{
    srand(time(NULL));
    int matriz[2][2];
    int m = 2;
    int n = 2;
    int repeticoes[5];
    int i, j, k, l;

    for ( i = 0; i < m; i++)
    {
        for ( j = 0; j < n; j++)
        {
            matriz[i][j] = rand() % 2 + 1;
            printf("%d\n", matriz[i][j]);
        }
    }
    for ( i = 0; i < m; i++)
    {
        for ( j = 0; j < n; j++)
        {
            int totalRepeticoes = 0;
            int index = 0;
            int jaTem = 0;
            for ( k = i; k < m; k++)
            {
                if(k == i){
                    l=j+1;
                }
                else{
                    l=0;
                }
                for ( l; l < n; l++)
                {
                    if (matriz[i][j] == matriz[k][l])
                    {
                        if (totalRepeticoes == 0)
                        {
                            while (repeticoes[index] != '\0')
                            {
                                jaTem = 0;
                                if (matriz[i][j] == repeticoes[index])
                                {
                                    jaTem = 1;
                                }
                                index++;
                            }
                        }
                    }
                    if (jaTem == 0)
                    {
                        repeticoes[index] = matriz[i][j];
                        totalRepeticoes = 1;
                        jaTem = 1;
                    }

                    else
                    {
                        totalRepeticoes++;
                    }
                }
            }
            if (totalRepeticoes != 0)
            {
                printf("O número %d se repetiu %d vezes\n", matriz[i][j], totalRepeticoes);
            }
        }
    }
}
