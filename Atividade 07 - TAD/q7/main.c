#include "racional.h"
#include <stdio.h>
#include <stdlib.h>

void lerRacional(int *numerador, int *denominador)
{
    printf("Numerador: ");
    scanf("%d", numerador);
    printf("Denominador: ");
    scanf("%d", denominador);
}

void main()
{
    Racional *racional;
    int op;
    int denominador, numerador;
    char *charPont = (char *)malloc(sizeof(char) * 50);

    lerRacional(&numerador, &denominador);
    Racional *racional1 = criarRacional(numerador, denominador);
    lerRacional(&numerador, &denominador);
    Racional *racional2 = criarRacional(numerador, denominador);
    Racional *soma = criarRacional(numerador, denominador);
    Racional *produto = criarRacional(numerador, denominador);

    soma = somar(racional1, racional2);
    printf("Soma: ");
    imprimirRacional(soma);

    produto = multiplicar(racional1, racional2);
    printf("Produto: ");
    imprimirRacional(produto);
    
    verifica(racional1, racional2);
}
