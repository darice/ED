// Crie uma função que, dado determinado vetor V de ordem ímpar alocado
// dinamicamente, imprima os seus valores “do centro para fora” e logo após libere
// a memória antes ocupada por V.

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int * criarVetor(int n){
    int * pont;
    pont = (int *) calloc(n, sizeof(int));
    return pont;
}

void imprimirVetor(int * vetor, int n){
    for(int i=0; i<n; i++){
        printf("%d, ", vetor[i]);
    }
}

void imprimirVetorDentroParaFora(int * vetor, int n){
    int metade = (n-1)/2;
    printf("%d\n", vetor[metade]);
    int i= 1;
    while(metade - i >= 0){
        printf("%d, %d\n", vetor[metade-i], vetor[metade+i]);
        i++;
    }
}

void liberarVetor(int *vetor){
    free(vetor);
}

int main(){
    int n = 7;
    int * vetor = criarVetor(n);
    srand(time(NULL));
    for(int i=0; i<n; i++){
        vetor[i] = rand() % 100;
    }
    imprimirVetor(vetor, n);
    printf("\n\n");
    imprimirVetorDentroParaFora(vetor, n);
    liberarVetor(vetor);
}
