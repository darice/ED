// Implemente uma função recursiva que, dados dois números inteiros x e n, calcula o
// valor de x^n .

#include <stdio.h>

int expo(int b, int p){
    if(p == 1){
        return b;
    }
    if(p == 0){
        return 1;
    }
    return b * expo(b, p-1);
}

void main(){
    printf("%d", expo(3,3));
}