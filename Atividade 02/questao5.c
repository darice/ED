// 2 – Desenvolva um algoritmo que efetue a troca de 2 números lidos do
// usuário, usando uma função do tipo void, retorne os dois numeros trocados.
// Mostre o antes e o depois do resultado da troca.

#include <stdio.h>

void troca(int *a, int *b)
{
    int aux = *a;
    *a = *b;
    *b = aux;
}

void main()
{
    int a;
    int b;

    scanf("%d", &a);
    scanf("%d", &b);

    int *aPont = &a;
    int *bPont = &b;

    printf("antes: a: %d, b: %d\n", a, b);
    troca(&a, &b);
    printf("depois: a: %d, b: %d\n", a, b);
}