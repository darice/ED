#include "conjunto.h"

void main()
{
    // • criar um conjunto vazio;
    Conjunto *conjunto3 = ler(0);

    // ler os dados de um conjunto;
    Conjunto *conjunto1 = ler(3);
    Conjunto *conjunto2 = ler(3);

    // fazer a união de dois conjuntos
    uniao(conjunto1, conjunto2);

    // fazer a interseção de dois conjuntos;
    intersecao(conjunto1, conjunto2);

    // verificar se dois conjunto são iguais
    igual(conjunto1, conjunto2);

    // imprimir um conjunto;
    imprimir(conjunto1);
}