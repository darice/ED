#include <stdio.h>
#include "biblioteca.h"
#include <string.h>
#include <stdlib.h>

void menu(int *op)
{
    do
    {
        printf("\nEscolha uma opção:\n");
        printf("1-Criarlivro;\n2-obtem Genero;\n3-obtem autor;\n4-obtem titulo;\n5-obtem ano;\n6-Verificar\n7-Sair.\n");
        scanf("%d", &(*op));
    } while ((*op) < 1 || (*op) > 7);
}

void main()
{
    Livro *livro;
    int op;
    char titulo[50];
    char autor[30];
    char genero[10];
    int ano;
    char *charPont = (char *)malloc(sizeof(char) * 50);

    menu(&op);
    do
    {
        switch (op)
        {
        case 1:
            printf("Titulo: ");
            scanf("%s", titulo);
            printf("Autor: ");
            scanf("%s", autor);
            printf("Genero: ");
            scanf("%s", genero);
            printf("Ano: ");
            scanf("%d", &ano);
            livro = livroCria(titulo, autor, genero, ano);
            break;
        case 2:
            charPont = livroObtemGenero(livro);
            printf("%s", charPont);
            break;
        case 3:
            charPont = livroObtemAutor(livro);
            printf("%s", charPont);
            break;
        case 4:
            charPont = livroObtemTitulo(livro);
            printf("%s", charPont);
            break;
        case 5:
            printf("%d", livroObtemAno(livro));
            break;
        case 6:
            printf("%d", verifica(livro));
            break;
        }
        menu(&op);
    } while (op != 7);
}
