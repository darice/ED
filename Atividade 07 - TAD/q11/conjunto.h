#ifndef conjunto_h
#define conjunto_h

#include <stdio.h>
#include <stdlib.h>

typedef struct conjunto Conjunto;

Conjunto *ler(int tamanho);
void imprimir(Conjunto *conjunto);
void uniao(Conjunto *conjunto1, Conjunto *conjunto2);
void intersecao(Conjunto *conjunto1, Conjunto *conjunto2);
void igual(Conjunto *conjunto1, Conjunto *conjunto2);

#endif