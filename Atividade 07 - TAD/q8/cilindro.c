#include<stdio.h>
#include<stdlib.h>
#include "cilindro.h"

struct cilindro{
    float raio, altura;
};

Cilindro *criar(float raio, float altura){
     Cilindro *cilindro;
    cilindro = (Cilindro*) calloc(1, sizeof(Cilindro));
    cilindro->raio=raio;
    cilindro->altura=altura;
}

void area(Cilindro*cilindro){
    float area = 2*3.14*cilindro->raio * cilindro->raio + 2*3.14*cilindro->raio*cilindro->altura;
    printf("Area: %f\n", area);
}

void volume(Cilindro*cilindro){
    float volume = 3.14*cilindro->raio*cilindro->raio*cilindro->altura;
    printf("Volume: %f\n", volume);
}