// Crie um programa que aloque dinamicamente um vetor de 1500 posições,
// inicialize-as com os valores de seus índices e logo após, imprima o valor das 10
// primeiras e das 10 ultimas posições do vetor.

#include<stdio.h>
#include<stdlib.h>

int * criarVetor(int n){
    int * pont;
    pont = (int *) calloc(n, sizeof(int));
    return pont;
}

void preencherVetor(int * vetor, int n){
    for(int i=0; i<n; i++){
        vetor[i] = i;
    }
}

void liberarVetor(int *vetor){
    free(vetor);
}

int main(){
    int n = 1500;
    int * vetor = criarVetor(n);
    preencherVetor(vetor, n);

    for(int i=0; i<10; i++){
        printf("%d, ", vetor[i]);
    }
    printf("\n");
    
    for(int i=n-10; i<n; i++){
        printf("%d, ", vetor[i]);
    }
   
    liberarVetor(vetor);
}