// Faça um programa que crie dinamicamente uma matriz de números do tipo float.
// Depois preencha a matriz com números digitados pelo usuário.

#include <stdio.h>
#include <stdlib.h>

float **criarMatrizDinamica(int m, int n)
{
    float **pont;
    pont = (float **)malloc(m * sizeof(float *));
    for (int i = 0; i < m; i++)
    {
        pont[i] = (float *)malloc(n * sizeof(float));
    }
    return pont;
}

void preencherMatriz(int m, int n,  float ** matriz){
    for(int i=0; i<m; i++){
        for (int j=0; j<n; j++){
            scanf("%f", &matriz[i][j]);
        }
    }
}

void imprimirMatriz(int m, int n, float **matriz){
    for(int i=0; i<m; i++){
        for (int j=0; j<n; j++){
            printf("%f, ", matriz[i][j]);
        }
    }
}


int main()
{
    int m, n;
    scanf("%d %d", &m, &n);
    float **matrizDinamica = criarMatrizDinamica(m, n);
    preencherMatriz(m, n, matrizDinamica);
    imprimirMatriz(m, n, matrizDinamica);

}