#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int matriz[2][2];
int m = 2;
int n = 2;
int tamanhoVetorNaoApareceu = 0;
int naoApareceu[5];

int main()
{
    srand(time(NULL));

    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            matriz[i][j] = rand() % 5 + 1;
            printf("%d\n", matriz[i][j]);
        }
    }

    for (int x = 1; x < 6; x++)
    {
        int totalRepeticoes = 0;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (x == matriz[i][j])
                {
                    totalRepeticoes++;
                }
            }
        }
        if (totalRepeticoes > 1)
        {
            printf("%d repetiu %d vezes\n", x, totalRepeticoes);
        }
        if (totalRepeticoes == 0)
        {
            naoApareceu[tamanhoVetorNaoApareceu] = x;
            tamanhoVetorNaoApareceu++;
        }
    }
        printf("Não tiveram ocorrências: ");
        for (int i; i < tamanhoVetorNaoApareceu; i++)
        {
            printf("%d, ", naoApareceu[i]);
        }
}