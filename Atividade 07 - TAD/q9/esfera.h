#ifndef esfera_h
#define esfera_h

typedef struct esfera Esfera;

Esfera *criar(float raio);
void area(Esfera*esfera);
void volume(Esfera*esfera);

#endif