#ifndef cilindro_h
#define cilindro_h

typedef struct cilindro Cilindro;

Cilindro *criar(float raio, float altura);
void area(Cilindro*cilindro);
void volume(Cilindro*cilindro);

#endif