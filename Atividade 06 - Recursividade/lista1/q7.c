// Ainda levando em conta o exemplo acima citado, crie uma função recursiva que produza a
// saída oposta.

#include<stdio.h>

void abc(char *s){
  if (s[0] == '\0'){
    return;
  } 
  printf("%c", s[0]);
  abc(s + 1);
}

void main(){
  char a[] = {'d','a','r','i','c','e'};
  abc(a);
}