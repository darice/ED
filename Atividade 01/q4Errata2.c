
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int tamanhoVetorJaFoi = 0;
int jaFoi[5];
int matriz[2][2];
int m = 2;
int n = 2;

void contarRepeticoes(int num)
{
    int totalAparicoes = 0;
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            printf("if %d == %d\n", num, matriz[i][j]);
            if (num == matriz[i][j])
            {
                if (totalAparicoes == 0)
                {
                    jaFoi[tamanhoVetorJaFoi] = num;
                    tamanhoVetorJaFoi++;
                }
                totalAparicoes++;
            }
        }
    }
    if (totalAparicoes > 1)
    {
        printf("Número %d se repetiu %d vezes\n", num, totalAparicoes);
    }
}

int verificaSeJaFoi(int num)
{
    for (int k = 0; k < tamanhoVetorJaFoi; k++)
    {
        if (num == jaFoi[k])
        {
            return 1;
        }
    }
    return 0;
}

void naoApareceram()
{
    for (int i = 1; i < 6; i++)
    {
        int apareceu = 0;
        for (int j = 0; j < tamanhoVetorJaFoi; j++)
        {
            // printf("if%d == %d\n", i, jaFoi[j]);
            if (i == jaFoi[j])
            {
                apareceu = 1;
                break;
            }
        }
        if (apareceu == 0)
        {
            printf("%d\n, ", i);
        }
    }
    printf("tamanho: %d\n", tamanhoVetorJaFoi);
}

int main()
{
    srand(time(NULL));

    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            matriz[i][j] = rand() % 5 + 1;
            printf("%d\n", matriz[i][j]);
        }
    }

    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {

            if (verificaSeJaFoi(matriz[i][j]))
            {
                break;
            }
            else
            {
                contarRepeticoes(matriz[i][j]);
            }
        }
    }
    printf("Não tiveram ocorrencias: ");
    naoApareceram();
    for (int i = 0; i < tamanhoVetorJaFoi; i++)
    {
        printf("%d ", jaFoi[i]);
    }
}