// Escreva uma estrutura chamada pessoa que deve conter cinco atributos nome, idade,
// peso, altura e pessoa.imc.
// Faça uma função que deve calcular o Índice de Massa Corporal (pessoa.imc) de uma pessoa,
// tendo como parâmetros o peso e a altura, retornando o resultado. A formula para o
// cálculo é a seguinte: pessoa.imc = peso * altura2.
// Na função principal crie um vetor de ponteiros para estrutura pessoa, preencha esse
// vetor, sendo que o pessoa.imc deve ser preenchido automaticamente com a chamada da função
// que realiza seu cálculo. De acordo com a tabela de classificação, informe os resultados
// em porcentagem para cada categoria

#include <stdio.h>

struct Pessoa
{
    char nome[100];
    int idade;
    float peso;
    float altura;
    float imc;
};

void main()
{

    struct Pessoa pessoa;

    printf("Nome: ");
    scanf("%s", pessoa.nome);
    printf("Idade: ");
    scanf("%d", &pessoa.idade);
    printf("peso: ");
    scanf("%f", &pessoa.peso);
    printf("altura: ");
    scanf("%f", &pessoa.altura);

    pessoa.imc = pessoa.peso / (pessoa.altura * pessoa.altura);

    if (pessoa.imc < 18.5)
    {
        printf("Abaixo do peso.\n");
    }
    else if (pessoa.imc > 18.5 && pessoa.imc < 24.9)
    {
        printf("Peso normal.\n");
    }
    else if (pessoa.imc > 25 && pessoa.imc < 29.9)
    {
        printf("Levemente acima do peso.\n");
    }
    else if (pessoa.imc > 30 && pessoa.imc < 34.9)
    {
        printf("Obeso.\n");
    }
    else if (pessoa.imc > 35 && pessoa.imc < 39.9)
    {
        printf("Obesidade Severa.\n");
    }
    else if(pessoa.imc > 40)
    {
        printf("Obesidade MORBIDA.\n");
    }
}
