// Escreva um programa em linguagem C que solicita ao usuário a quantidade de
// alunos de uma turma e aloca um vetor de notas (números reais). Depois de ler as
// notas, imprime a média aritmética. Não deve ocorrer desperdício de memória; e após
// ser utilizada a memória deve ser desalocada.

#include<stdio.h>
#include<stdlib.h>

float * criarVetor(int n){
    float * pont;
    pont = (float *) calloc(n, sizeof(float));
    return pont;
}

float *preencherVetor(float * vetor, int n){
    for(int i=0; i<n; i++){
       scanf("%f", &vetor[i]);
    }
return vetor;
}


float media(float * vetor, int n){
    float soma = 0;
    for(int i=0; i<n; i++){
       soma += vetor[i];
    }
    float media = soma/n;
return media;
}

void liberarVetor(float *vetor){
    free(vetor);
}

int main(){
    int n;
    scanf("%d", &n);
    float * vetor = criarVetor(n);
    vetor = preencherVetor(vetor, n);
    float mediaAritmetica = (media(vetor, n));
    printf("%.2f", mediaAritmetica);
    liberarVetor(vetor);
}