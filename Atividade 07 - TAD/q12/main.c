#include "aluno.h"

void main()
{
    int tamanho = 3;
    Aluno *alunos = (Aluno *)calloc(tamanho, sizeof(Aluno*));
    preencher(alunos, 0, 123, 'p', "maria", 10, 8, 3);
    preencher(alunos, 1, 314, 'p', "joao", 5, 6, 7);
    preencher(alunos, 2, 245, 'p', "pedro", 8, 9, 10);

    // buscar aluno com matricula 245
    buscar(alunos, tamanho, 245);

    // ordenar alunos por ordem crescente da matricula
    ordenar(alunos, tamanho);

    // imprimirTodos os alunos
    imprimir(alunos, tamanho);
}