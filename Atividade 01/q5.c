// Imagine um matriz de M x N preenchida com zeros. Calcular o maior circulo inscrito nessa matriz.
// Todas as posições que pertencerem ao circulo devera ter valor 1.

#include <stdio.h>
int matriz[6][6];
int matrizParte[3][3];
int m = 6;
int n = 6;
int raio = 6 / 2;

void parte1()
{
    int raio2 = raio - 1;
    for (int x = 0; x < raio; x++)
    {
        for (int i = 0; i < raio; i++)
        {
            for (int j = 0; j < raio; j++)
            {
                if (i + j == raio2)
                {
                    matrizParte[i][j] = 1;
                }
            }
        }
        raio2++;
    }
}

void parte2()
{
    int raio2 = raio - 1;
    for (int x = 0; x < raio; x++)
    {
        for (int i = 0; i < raio; i++)
        {
            for (int j = 0; j < raio; j++)
            {
                if (i + j == raio2)
                {
                    matrizParte[i][j] = 1;
                }
            }
        }
        raio2--;
    }
}

void parte3()
{
    for (int x = 0; x < raio; x++)
    {
        for (int i = 0; i < raio; i++)
        {
            for (int j = 0; j < raio; j++)
            {
                if (i - j == x)
                {
                    matrizParte[i][j] = 1;
                }
            }
        }
        
    }
}

void parte4()
{
    for (int x = 0; x < raio; x++)
    {
        for (int i = 0; i < raio; i++)
        {
            for (int j = 0; j < raio; j++)
            {
                if (j - i == x)
                {
                    matrizParte[i][j] = 1;
                }
            }
        }
    }
}

void preencherMatrizcomMatrizParte(int iInicial, int jInicial){
    for(int i=0 ; i<raio; i++ ){
        for(int j=0; j<raio; j++){
            matriz[i+ iInicial][j+ jInicial] = matrizParte[i][j];
        }
    }
}



void imprimirMatriz()
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            printf("%d ", matriz[i][j]);
        }
        printf("\n");
    }
}

void criarMatriz()
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            matriz[i][j] = 0;
        }
    }
}

void limparMatrizParte()
{
    for (int i = 0; i < raio; i++)
    {
        for (int j = 0; j < raio; j++)
        {
            matrizParte[i][j] = 0;
        }
    }
}

int main()
{
    criarMatriz();
    limparMatrizParte();
    parte1();
    preencherMatrizcomMatrizParte(0,0);
    limparMatrizParte();
    parte2();
    preencherMatrizcomMatrizParte(raio,raio);   
    limparMatrizParte();
    parte3();
    preencherMatrizcomMatrizParte(0,raio);  
    limparMatrizParte();
    parte4();
    preencherMatrizcomMatrizParte(raio,0);  
    imprimirMatriz();
}
