#ifndef racional_h
#define racional_h

typedef struct racional Racional;

Racional *criarRacional(int numerador, int denominador);
Racional *somar(Racional *racional1, Racional *racional2);
Racional *multiplicar(Racional *racional1, Racional *racional2);
void *verifica(Racional *racional1, Racional *racional2);
void imprimirRacional(Racional *racional);

#endif