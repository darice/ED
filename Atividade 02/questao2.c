// Crie um algoritmo que efetue a troca de dois valores informados pelo usuário
// utilizando ponteiros. 

#include <stdio.h>

void main(){
    int a;
    int b;

    scanf("%d", &a);
    scanf("%d",&b);

    int *aPont = &a;
    int *bPont = &b;

    int aux = *aPont;
    *aPont = *bPont;
    *bPont = aux;

    printf("%d %d",a, b);

}