// Desenvolva um algoritmo que leia dois valores informados pelo usuário.
// Depois, usar ponteiros para fazer a soma sem a acessar o valor das varáveis
// diretamente.

#include <stdio.h>

void main()
{
    int a;
    int b;
    int soma;
    int *aPont = &a;
    int *bPont = &b;

    scanf("%d", &a);
    scanf("%d", &b);

    soma = *aPont + *bPont;
    printf("%d", soma);
}