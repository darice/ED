// Escreva uma função que faça a procura sequencial de um
// valor passado por parâmetro num vetor também passado por
// parâmetro.

#include <stdio.h>

void buscar(int vet[], int tamanho, int valor){
    tamanho -= 1;
    if(vet[tamanho] == valor){
        printf("encontrado na posicao %d", tamanho);
        return;
    }
    if(tamanho == 0){
        printf("Não encontrado");
        return;
    }
    return buscar(vet, tamanho, valor);
}

void main(){
    int vetor[]= {1,4,7,3,6};
    buscar(vetor, 5, 3);
}