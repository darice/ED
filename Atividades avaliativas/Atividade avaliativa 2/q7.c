#include <stdio.h>
#include <stdlib.h>
#include <math.h>

FILE *file;

#define tam 225

typedef struct ponto ponto;
typedef struct Grupo Grupo;
struct ponto
{
    int x, y, valor;
};
struct Grupo
{
    ponto semente;
    ponto *elementos;
    int rotulo; //valor que identifica os pontos pertencentes ao grupo
    int qtdelementos;
};

int calcDistancia(int x1, int y1, int x2, int y2)
{
    return (int)sqrt(pow(x2 - x1, 2.0) + pow(y2 - y1, 2.0));
}

ponto *adicionarSemente(ponto *p, int posicao, int x, int y)
{
    p[posicao].x = x;
    p[posicao].y = y;
    return p;
}

void main()
{
    ponto *sementes;
    sementes = (ponto *)calloc(7, sizeof(ponto));

    sementes = adicionarSemente(sementes, 0, 50, 50);
    sementes = adicionarSemente(sementes, 1, 38, 101);
    sementes = adicionarSemente(sementes, 2, 129, 60);
    sementes = adicionarSemente(sementes, 3, 104, 163);
    sementes = adicionarSemente(sementes, 4, 195, 132);
    sementes = adicionarSemente(sementes, 5, 168, 177);
    sementes = adicionarSemente(sementes, 6, 15, 15);

    int **mat;
    mat = (int **)calloc(tam, sizeof(int *));

    for (int j = 0; j < tam; j++)
    {
        mat[j] = (int *)calloc(tam, sizeof(int));
    }

    file = fopen("matriz.txt", "r");
    if (file == NULL)
    {
        printf("Arquivo não encontrado");
        exit(0);
    }
    for (int i = 0; i < tam; i++)
    {
        for (int j = 0; j < tam; j++)
        {
            fscanf(file, "%d", &mat[i][j]);
        }
    }
    fclose(file);

    Grupo *grupo;

    int l = 2;
    int contGrupos = 0;
    int contTotal = 0;

    int ok = 0;
    for(int n=0; n<7; n++)
    {
        int cont = 0;

        int sementeX = sementes[n].x;
        int sementeY = sementes[n].y;
    
        if (contGrupos == 0)
        {
            grupo = (Grupo *)calloc(1, sizeof(Grupo));
            contGrupos = 1;
        }
        else
        {
            contGrupos++;
            grupo = (Grupo *)realloc(grupo, sizeof(Grupo) * contGrupos);
        }

        grupo[contGrupos - 1].rotulo = 256 + contGrupos;
        grupo[contGrupos - 1].elementos = calloc(1, sizeof(Grupo));
        grupo[contGrupos - 1].elementos[0].x = sementeX;
        grupo[contGrupos - 1].elementos[0].y = sementeY;
        grupo[contGrupos - 1].elementos[0].valor = mat[sementeX][sementeY];
        mat[sementeX][sementeY] = grupo[contGrupos - 1].rotulo;
        cont = 1;

        grupo[contGrupos - 1].semente.x = sementeX;
        grupo[contGrupos - 1].semente.y = sementeY;
        grupo[contGrupos - 1].semente.valor = mat[sementeX][sementeY];

        for (int i = 0; i < tam; i++)
        {
            for (int j = 0; j < tam; j++)
            {
                if (mat[i][j] < 256)
                {

                    if (calcDistancia(sementeX, sementeY, i, j) <= l)
                    {
                        cont++;
                        grupo[contGrupos - 1].elementos = realloc(grupo[contGrupos - 1].elementos, sizeof(Grupo) * cont);
                        grupo[contGrupos - 1].elementos[cont - 1].x = i;
                        grupo[contGrupos - 1].elementos[cont - 1].y = j;
                        grupo[contGrupos - 1].elementos[cont - 1].valor = mat[i][j];
                        mat[i][j] = grupo[contGrupos - 1].rotulo;
                    }
                }
            }
        }
        grupo[contGrupos - 1].qtdelementos = cont;
        contTotal += cont;

    }

    for (int i = 0; i < contGrupos; i++)
    {
        printf("\nSemente: (%d,%d)\n", grupo[i].semente.x, grupo[i].semente.y);
        printf("Rotulo: %d\n", grupo[i].rotulo);
        for (int j = 0; j < grupo[i].qtdelementos; j++)
        {
            printf("(%d,%d) = %d\n", grupo[i].elementos[j].x, grupo[i].elementos[j].y, grupo[i].elementos[j].valor);
        }
    }
}
