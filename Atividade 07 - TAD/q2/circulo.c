#include <stdio.h>
#include "circulo.h"
#include <math.h>

struct Ponto
{
    int x, y;
};
typedef struct Ponto Ponto;

struct Circulo
{
    Ponto centro;
    int raio;
};


Circulo* criar(Circulo *circulo, int x, int y, int raio)
{
    circulo->centro.x = x;
    circulo->centro.y = y;
    circulo->raio = raio;
    return circulo;
}

void liberar(Circulo *circulo)
{
    free(circulo);
}

void calcularArea(Circulo *circulo, float *area)
{
    *area = circulo->raio * circulo->raio * 3.14;
}

int distancia(int x1, int y1, int x2, int y2)
{
    int distancia = sqrt(pow(x1 - x2, 2) + pow(x1 - x2, 2));
}

void interior(Circulo *circulo, int x, int y)
{
    if (distancia(circulo->centro.x, circulo->centro.y, x, y) <= circulo->raio)
    {
        printf("Pertence ao circulo");
    }
    else
    {
        printf("Não pertence ao circulo");
    }
}
