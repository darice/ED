// Crie uma struct que represente alunos, com nome, matrícula e um vetor com 3 notas. Construa
// um programa para auxiliar um professor na atribuição de notas e separação entre aprovados(média
// de notas >= 7) e reprovados. Seu programa deve receber, em tempo de execução, a quantidade de
// alunos da disciplina para que seja alocado um espaço adequado. Em seguida, disponibilize ao
// professor as opções de cadastrar alunos, atribuir nota, imprimir aprovados e/ou reprovados.

#include <stdio.h>
#include <stdlib.h>

struct alunos
{
    char *nome;
    int matricula;
    float notas[3];
};

int menu()
{
    int op;
    printf("---------MENU----------\n");
    printf("0 - Sair\n");
    printf("1 - Cadastrar alunos\n");
    printf("2 - Cadastrar notas dos alunos cadastrados\n");
    printf("3 - Mostrar alunos aprovados\n");
    printf("4 - Mostrar alunos reprovados\n");
    printf("5 - Mostrar todos os alunos\n");
    scanf("%d", &op);
    return op;
}

char *alocarChar(int t)
{
    char *string = (char *)malloc(sizeof(char) * t);
    return string;
}

struct alunos *cadastrarAlunos(struct alunos *a, int *quantidade)
{
    struct alunos *alunosNovos = (struct alunos *)realloc(a, sizeof(struct alunos) * (1 + (*quantidade)));
    printf("Nome ");
    alunosNovos[*quantidade].nome = alocarChar(100);
    scanf("%s", alunosNovos[*quantidade].nome);
    printf("Matricula ");
    scanf("%d", &alunosNovos[*quantidade].matricula);
    (*quantidade)++;
    return alunosNovos;
}

void *cadastrarNotas(struct alunos *a, int quantidadeAlunos, int *quantidadeNotasCadastradas)
{
    int i = *quantidadeNotasCadastradas;
    for (i; i < quantidadeAlunos; i++)
    {
        printf("\nNome: %s\n", a[i].nome);
        printf("Matricula: %d\n", a[i].matricula);
        for (int j = 0; j < 3; j++)
        {
            printf("Nota %d: ", j + 1);
            scanf("%f", &a[i].notas[j]);
        }
    }
    *quantidadeNotasCadastradas = quantidadeAlunos;
}

void mostrarAlunos(struct alunos *alunos, int quantAlunos)
{
    for (int i = 0; i < quantAlunos; i++)
    {
        printf("Nome: %s\n", alunos[i].nome);
        printf("Matricula: %d\n", alunos[i].matricula);
        printf("Notas: %.2f, %.2f, %.2f\n\n", alunos[i].notas[0], alunos[i].notas[1], alunos[i].notas[2]);
    }
}

void mostrarAlunosAprovados(struct alunos *alunos, int quantAlunos)
{
    printf("Aprovados:\n");
    for (int i = 0; i < quantAlunos; i++)
    {
        float media = alunos[i].notas[0] + alunos[i].notas[1] + alunos[i].notas[2];
        media /= 3.0;
        if (media >= 7)
        {
            printf("Nome: %s\n", alunos[i].nome);
            printf("Matricula: %d\n\n", alunos[i].matricula);
        }
    }
}

void mostrarAlunosReprovados(struct alunos *alunos, int quantAlunos)
{
    printf("Reprovados:\n");
    for (int i = 0; i < quantAlunos; i++)
    {
        float media = alunos[i].notas[0] + alunos[i].notas[1] + alunos[i].notas[2];
        media /= 3.0;
        if (media < 7)
        {
            printf("Nome: %s\n", alunos[i].nome);
            printf("Matricula: %d\n\n", alunos[i].matricula);
        }
    }
}

void main()
{
    struct alunos *alunos = NULL;

    int quantAlunos = 0;
    int quantNotasCadastradas = 0;
    int op = menu();

    while (1)
    {
        switch (op)
        {
        case 1:
            alunos = cadastrarAlunos(alunos, &quantAlunos);
            break;
        case 2:
            cadastrarNotas(alunos, quantAlunos, &quantNotasCadastradas);
            break;
        case 3:
            mostrarAlunosAprovados(alunos, quantAlunos);
            break;
        case 4:
            mostrarAlunosReprovados(alunos, quantAlunos);
            break;
        case 5:
            mostrarAlunos(alunos, quantAlunos);
            break;
        case 0:
            free(alunos);
            exit(0);
            break;
        default:
            printf("Valor invalido \n");
            break;
        }
        op = menu();
    };
}