// Crie um algoritmo que receba do usuário a quantidade de elementos a ser
// inserida em um vetor de inteiros. Logo após, preencha este vetor, imprima os
// seus elementos e exiba a soma dos seus elementos impares.

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int * criarVetor(int n){
    int * pont;
    pont = (int *) calloc(n, sizeof(int));
    return pont;
}

void imprimirVetor(int * vetor, int n){
    for(int i=0; i<n; i++){
        printf("%d, ", vetor[i]);
    }
}

void liberarVetor(int *vetor){
    free(vetor);
}

void somaImpares(int *vetor, int n){
    int soma = 0;
    for(int i=0; i<n; i++){
        if(vetor[i] % 2 == 1){
            soma += vetor[i];
        }
    }
    printf("Soma dos elementos impares: %d", soma);
}

int main(){
    int n;
    scanf("%d", &n);
    int * vetor = criarVetor(n);
    srand(time(NULL));
    for(int i=0; i<n; i++){
        vetor[i] = rand() % 10;
    }
    imprimirVetor(vetor, n);
    somaImpares(vetor, n);
    liberarVetor(vetor);
}