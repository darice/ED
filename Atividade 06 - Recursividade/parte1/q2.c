// Crie um programa em C que receba um vetor de números reais com 100
// elementos. Escreva uma função recursiva que inverta ordem dos
// elementos presentes no vetor.

#include <stdio.h>

#define N 100

void inverter(int vetor[], int x){
    int ate = N/2;
    if(x == ate){
        return;
    }
    int aux = vetor[N-x];
    vetor[N-x] = vetor[x-1];
    vetor[x-1] = aux;
    return inverter(vetor, x-1);
}

void main(){
    int vetor[N];
    for(int i=0; i<N; i++){
        vetor[i] = i;
    }
    inverter(vetor, N);
    for(int i=0; i<N; i++){
        printf("%d ", vetor[i]);
    }

}