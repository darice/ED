// (QUESTÃO DESAFIO) escreva uma função que, dado determinado vetor de no
// máximo 3 caracteres, seja capaz de devolver todos os anagramas possíveis, ou
// seja, todas as sequencias passíveis de formação com os caracteres inseridos no
// vetor. (Obs.: Não é necessário verificar a validade das palavras. Apenas exiba as
// possibilidades.)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *criarVetor(int n)
{
    char *pont;
    pont = (char *)calloc(n, sizeof(char));
    return pont;
}

void imprimirVetor(char *vetor, int n)
{
    if (n == 3)
    {
        if (vetor[0] == vetor[1] && vetor[1]== vetor[2])
        {
            printf("%c%c%c\n", vetor[0], vetor[1], vetor[2]);
        }
        else
        {
            if (vetor[0] == vetor[1])
            {
                char aux = vetor[0];
                vetor[0] = vetor[2];
                vetor[2] = aux;
            }
            else if (vetor[0] == vetor[2])
            {
                char aux = vetor[0];
                vetor[0] = vetor[1];
                vetor[1] = aux;
            }
            printf("%c%c%c\n", vetor[0], vetor[1], vetor[2]);
            printf("%c%c%c\n", vetor[1], vetor[2], vetor[0]);
            printf("%c%c%c\n", vetor[1], vetor[0], vetor[2]);
            if (vetor[1] != vetor[2])
            {
                printf("%c%c%c\n", vetor[0], vetor[2], vetor[1]);
                printf("%c%c%c\n", vetor[2], vetor[0], vetor[1]);
                printf("%c%c%c\n", vetor[2], vetor[1], vetor[0]);
            }
        }
    }
    if (n == 2)
    {
        printf("%c%c\n", vetor[0], vetor[1]);
        if (vetor[0] != vetor[1])
            printf("%c%c\n", vetor[1], vetor[0]);
    }
    if (n == 1)
    {
        printf("%c\n", vetor[0]);
    }
}

void liberarVetor(char *vetor)
{
    free(vetor);
}

int main()
{
    int n = 3;
    char *vetor = criarVetor(n);
    scanf("%s", vetor);
    n = strlen(vetor);
    imprimirVetor(vetor, n);
    liberarVetor(vetor);
}