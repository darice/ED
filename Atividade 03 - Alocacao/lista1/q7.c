// Implemente a função char *repete(char *s, int n) que cria dinamicamente uma nova
// string com n cópias da string original, separadas por espaço, exceto a última
// ocorrência. Exemplo: repete(“Ola”,4) -> “Ola Ola Ola Ola”.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *repete(char *s, int n)
{
    char *string;
    int posicaoFim = 0;

    string = (char *)calloc((strlen(s) + 1) * n, sizeof(char));
    for (int i = 0; i < n; i++)
    {
        if (i != 0)
        {
            string[posicaoFim] = ' ';
            posicaoFim++;
        }
        for (int j = 0; j < strlen(s); j++)
        {
            string[posicaoFim] = s[j];
            posicaoFim++;
        }
        posicaoFim++;
    }
    string[posicaoFim] = '\n';

    return string;
}
void main()
{
    // char string[] = "hello";
    char * stringNova = repete("Hello", 3);
    for (int i = 0; stringNova[i] != '\n'; i++)
    {
        printf("%c", stringNova[i]);
    }
}