//  Crie um programa que implemente o jogo “Bingo de Prog II”.Nesse jogo, o jogador
//  deve selecionar a quantidade de números que ele gostaria de apostar (entre 1 e 20), e
//  em seguida, informar os números escolhidos (valores entre 0 e 100). Após receber a
//  aposta, o computador sorteia 20 números (entre 0 e 100) e compara os números
//  sorteados com os números apostados, informando ao apostador a quantidade de
//  acertos e os números que ele acertou.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void ler_aposta(int *aposta, int n)
{
    int num;
    for (int i = 0; i < n; i++)
    {
        do
        {
            printf("Digite um número entre 0 e 100 para apostar: ");
            scanf("%d", &num);
        } while (num <= 0 || num > 100);
        aposta[i] = num;
    }
}

void sorteia_valores(int *sorteio, int n)
{
    srand(time(NULL));

    int numeros[100];
    for (int i = 0; i < 100; i++)
    {
        numeros[i] = i + 1;
    }

    for (int i = 0; i < 100; i++)
    {
        int posicaoAleatoria = rand() % 100;
        int aux = numeros[i];
        numeros[i] = numeros[posicaoAleatoria];
        numeros[posicaoAleatoria] = aux;
    }

    for (int i = 0; i < n; i++)
    {
        sorteio[i] = numeros[i];
    }
}

int *compara_aposta(int *aposta, int *sorteio, int *qtdacertos, int n, int ns)
{
    *qtdacertos = 0;
    int *comparaAposta = (int *)realloc(NULL, 1 * sizeof(int));

    for (int i = 0; i < ns; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (sorteio[i] == aposta[j])
            {
                comparaAposta[*qtdacertos] = sorteio[i];
                (*qtdacertos)++;
                j = n;
            }
        }
    }
    return comparaAposta;
}

void main()
{
    int n;
    printf("Digite a quantidade de números que deseja apostar: ");
    scanf("%d", &n);
    int *aposta = (int *)calloc(n, sizeof(int));
    ler_aposta(aposta, n);
    int *sorteio = (int *)calloc(20, sizeof(int));
    sorteia_valores(sorteio, 20);
    int quantAcertos;


    int *comparaAposta = compara_aposta(aposta, sorteio, &quantAcertos, n, 20);

    printf("Quantidade de acertos: %d\n", quantAcertos);
    for (int i = 0; i < quantAcertos; i++)
    {
        printf("%d, ", comparaAposta[i]);
    }
}