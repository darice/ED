#include "conjunto.h"

struct conjunto
{
    int tamanho;
    int conjunto[20];
};

Conjunto *ler(int tamanho)
{
    Conjunto *conjunto;
    conjunto = (Conjunto *)calloc(1, sizeof(Conjunto));
    for (int i = 0; i < tamanho; i++)
    {
        printf("Digite o elemento %d do conjunto: ", i + 1);
        scanf("%d", (conjunto->conjunto + i));
    }
    conjunto->tamanho = tamanho;
}

void imprimir(Conjunto *conjunto)
{
    for (int i = 0; i < (conjunto->tamanho); i++)
    {
        printf("%d ", conjunto->conjunto[i]);
    }
}

void uniao(Conjunto *conjunto1, Conjunto *conjunto2)
{
    printf("\nUnião: ");
    for (int i = 0; i < conjunto1->tamanho; i++)
    {
        for (int j = 0; j < conjunto2->tamanho; j++)
        {
            if (conjunto1->conjunto[i] == conjunto2->conjunto[j])
            {
                printf("%d ", conjunto1->conjunto[i]);
            }
        }
    }
}

void intersecao(Conjunto *conjunto1, Conjunto *conjunto2)
{
    printf("\nIntersecao: ");
    imprimir(conjunto1);
    for (int i = 0; i < conjunto2->tamanho; i++)
    {
        int ok = 0;
        for (int j = 0; j < conjunto1->tamanho; j++)
        {
            if (conjunto2->conjunto[i] == conjunto1->conjunto[j])
            {
                ok = 1;
            }
        }
        if (ok == 0)
        {
            printf("%d ", conjunto2->conjunto[i]);
        }
    }
}

void igual(Conjunto *conjunto1, Conjunto *conjunto2)
{
    for (int i = 0; i < conjunto2->tamanho; i++)
    {
        int ok = 0;
        for (int j = 0; j < conjunto1->tamanho; j++)
        {
            if (conjunto2->conjunto[i] == conjunto1->conjunto[j])
            {
                ok = 1;
            }
        }
        if (ok == 0)
        {
            printf("\nNão são iguais\n");
            return;
        }
    }
    printf("\nSão iguais\n");
}