// Escreva uma função recursiva que calcule a média dos valores de um vetor de
// inteiros.

#include <stdio.h>

float media(int vet[], int tamanho, int x){
    if(x == 0){
        return 0.0;
    }
    return (float) vet[x-1]/tamanho + media(vet, tamanho, x-1);
}

void main(){
    int tamanho = 3;
    int vetor[] = {6,2,9};
    printf("%f\n", media(vetor, tamanho, tamanho));
}