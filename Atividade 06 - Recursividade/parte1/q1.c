// Considere um sistema numérico que não tenha a operação de adição
// implementada e que você disponha somente dos operadores (funções)
// sucessor e predecessor. Então, pede-se para escrever uma função
// recursiva que calcule a soma de dois números x e y através desses dois
// operadores: sucessor e predecessor.
#include <stdio.h>

int sucessor(int n){
    return n+1;
}

int predecessor(int n){
    return n-1;
}

void soma(int a, int b){
    if(b == 0){
        printf("%d", a);
        return;
    }
    return soma(sucessor(a), predecessor(b));
}

void main(){
    soma(5,3);
}


