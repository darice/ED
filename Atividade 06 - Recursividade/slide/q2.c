// Escreva uma função recursiva que escreva na tela todos os
// números inteiros positivos desde um valor K informado pelo
// usuário até 0.

#include <stdio.h>

void sequencia(int k){
    printf("%d ",k);
    if(k != 0){
        return sequencia(k-1);
    }

}

void main(){
    sequencia(10);
}