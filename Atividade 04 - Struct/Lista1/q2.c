// Fazer um programa de ‘dialogo de login’ onde seja possível cadastrar no
// máximo 10 nomes de usuário e suas respectivas senhas (usuarios de usuário
// repetidos devem ser descartados). No diálogo de login, o programa deve
// testar se o usuário fornecido existe e se a sua senha confere
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Login
{
    char usuario[100];
    char senha[10];
};

typedef struct Login Login;

int cadastrar(int i, Login *login)
{
    printf("\nUsuario: ");
    scanf("%s", login[i].usuario);
    for (int j = 0; j < i; j++)
    {
        if (!strcmp(login[i].usuario, login[j].usuario))
        {
            printf("Usuario inválido\n");
            return 0;
        }
    }
    printf("Senha: ");
    scanf("%s", login[i].senha);
    return 1;
}

void main()
{
    Login *login;
    int n = 3;

    login = (Login *)calloc(n, sizeof(Login));

    for (int i = 0; i < n; i++)
    {
        if (!cadastrar(i, login))
        {
            i--;
        }
    }

    printf("\n\n*Login*\n");
    while (1)
    {
        char senha[10], usuario[100];
        printf("\nUsuario: ");
        scanf("%s", usuario);
        printf("Senha: ");
        scanf("%s", senha);
        for (int i = 0; i < n; i++)
        {
            if (!strcmp(login[i].usuario, usuario) && !strcmp(login[i].senha, senha))
            {
                printf("Sistema conectado");
                exit(0);
            }
        }
        printf("Credenciais invalidas\n");
    }
}