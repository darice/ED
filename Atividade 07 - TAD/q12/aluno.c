#include "aluno.h"

struct aluno
{
    int mat;
    char turma;
    char nome[81];
    float p1, p2, p3;
};

void preencher(Aluno *aluno, int posicao, int mat, char turma, char nome[], float p1, float p2, float p3)
{
    (aluno + posicao)->mat = mat;
    (aluno + posicao)->turma = turma;
    strcpy((aluno + posicao)->nome, nome);
    (aluno + posicao)->p1 = p1;
    (aluno + posicao)->p2 = p2;
    (aluno + posicao)->p3 = p3;
}

void buscar(Aluno *aluno, int tamanho, int matricula)
{
    for (int i = 0; i < tamanho; i++)
    {
        if (matricula == (aluno + i)->mat)
        {
            printf("\nAluno encontrado!");
            printf("\nmat: %d\n", (aluno + i)->mat);
            printf("turma: %c\n", (aluno + i)->turma);
            printf("nome: %s\n", (aluno + i)->nome);
            printf("nota1: %f\n", (aluno + i)->p1);
            printf("nota2: %f\n", (aluno + i)->p2);
            printf("nota3: %f\n", (aluno + i)->p3);
            return;
        }
    }
    printf("Aluno não encontrado!");
}

void ordenar(Aluno *aluno, int tamanho)
{
    int i, j;
    Aluno aux;
    for (i = 1; i < tamanho; i++)
    {
        aux = aluno[i];
        j = i - 1;
        while (j >= 0 && (aluno + j)->mat > aux.mat)
        {
            aluno[j + 1] = aluno[j];
            j--;
        }
        aluno[j + 1] = aux;
    }
}

void imprimir(Aluno *aluno, int tamanho)
{
    printf("\nALUNOS:\n");
    for (int i = 0; i < tamanho; i++)
    {
        printf("\nmat: %d\n", (aluno + i)->mat);
        printf("turma: %c\n", (aluno + i)->turma);
        printf("nome: %s\n", (aluno + i)->nome);
        printf("nota1: %f\n", (aluno + i)->p1);
        printf("nota2: %f\n", (aluno + i)->p2);
        printf("nota3: %f\n", (aluno + i)->p3);
    }
}
