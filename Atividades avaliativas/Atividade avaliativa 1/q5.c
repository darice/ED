// int** ocorrencias(int **mat, int l, int c);
// void mostrarOcorrencias(/*coloque os paraemtros que julgar necessário*/)

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int **alocarMatriz(int linha, int coluna)
{
    int **mat;
    int i;
    mat = (int **)calloc(linha, sizeof(int *));
    for (i = 0; i < linha; i++)
    {
        mat[i] = (int *)calloc(coluna, sizeof(int));
    }
    return mat;
}

int **preencherMatriz(int m, int n, int **matriz)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
            matriz[i][j] = rand() % 1001;
    }
    return matriz;
}

int **ocorrencias(int **matriz, int linha, int coluna, int *tamanhoOcorrencias)
{
    int **ocorrencias = (int **)calloc(2, sizeof(int *));
    (*tamanhoOcorrencias) = 0;

    for (int n = 0; n <= 1000; n++)
    {
        int cont = 0;
        for (int i = 0; i < linha; i++)
        {
            for (int j = 0; j < coluna; j++)
            {
                if (matriz[i][j] == n)
                {
                    cont++;
                }
            }
        }
        if (cont != 0)
        {
            (*tamanhoOcorrencias)++;
            if ((*tamanhoOcorrencias) == 1)
            {
                ocorrencias[0] = (int *)calloc(1, sizeof(int));
                ocorrencias[1] = (int *)calloc(1, sizeof(int));
            }
            else
            {
                ocorrencias[0] = (int *)realloc(ocorrencias[0], (*tamanhoOcorrencias) * sizeof(int));
                ocorrencias[1] = (int *)realloc(ocorrencias[1], (*tamanhoOcorrencias) * sizeof(int));
            }
            ocorrencias[0][(*tamanhoOcorrencias) - 1] = n;
            ocorrencias[1][(*tamanhoOcorrencias) - 1] = cont;
        }
    }
    return ocorrencias;
}

void main()
{
    srand(time(NULL));
    int linha, coluna;
    int **matriz;
    int **ocorrenciasMat;
    int tamanhoOcorrencias;
    printf("Digite o numero de linha e coluna da matriz: ");
    scanf("%d", &linha);
    scanf("%d", &coluna);
    matriz = alocarMatriz(linha, coluna);
    matriz = preencherMatriz(linha, coluna, matriz);
    ocorrenciasMat = ocorrencias(matriz, linha, coluna, &tamanhoOcorrencias);

    for (int j = 0; j<tamanhoOcorrencias !='\0'; j++)
    {
        printf("%d apareceu %d vez(es)\n", ocorrenciasMat[0][j], ocorrenciasMat[1][j]);
    }

    for (int i = 0; i < coluna; i++)
    {
        free(matriz[i]);
    }
    free(matriz);

    for (int i = 0; i < tamanhoOcorrencias; i++)
    {
        free(ocorrenciasMat[i]);
    }
    free(ocorrenciasMat);
}
