// Construa um programa (main) que aloque em tempo de execução (dinamicamente)
// uma matriz de ordem m x n (linhas por colunas), usando 2 chamadas a função malloc.

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int linhas, colunas;
    scanf("%d %d", &linhas, &colunas);

int **matriz ;

matriz = malloc (linhas * sizeof (int*)) ;

matriz[0] = malloc (linhas * colunas * sizeof (int)) ;


for (int i = 1; i < linhas; i++)
   matriz[i] = matriz[0] + i * colunas ;
}