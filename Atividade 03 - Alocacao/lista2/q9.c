
// Construa uma struct que represente dispositivos eletrônicos, contendo descrição e valor. Faça
// outra struct que represente pessoas, contendo nome, cpf e um ponteiro para um vetor do tipo
// dispositivos eletrônicos. Faça funções para criar e apagar dispositivos eletrônicos alocados
// dinamicamente. Faça também funções para armazenar e deletar pessoas cuja quantidade de
// dispositivos eletrônicos é passada em tempo de execução. Teste as funções criadas no método main.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Dispositivo
{
  char *descricao;
  int valor;
};

struct Pessoa
{
  char *nome;
  int cpf;
  int quantDispositivos;
  struct Dispositivo *dispositivo;
};

char *alocarChar(int t)
{
  char *string = (char *)malloc(sizeof(char) * t);
  return string;
}

int menu()
{
  printf("\n0 - Sair\n");
  printf("1 - Cadastrar pessoa\n");
  printf("2 - Cadastrar dispositivo\n");
  printf("3 - Mostrar pessoas e dispositivos\n");
  printf("4 - Deletar pessoa\n");
  printf("5 - Deletar dispositivo\n");
  printf("Opcao: ");
  int opcao;
  scanf("%d", &opcao);
  return opcao;
}

void mostrarPessoasDispositivos(struct Pessoa *pessoas, int tamanho, int mostrarDispositivo)
{
  int idPessoa, idDispositivo;
  for (idPessoa = 0; idPessoa < tamanho; idPessoa++)
  {
    printf("%d - %s\tCPF: %d\n", idPessoa + 1, pessoas[idPessoa].nome, pessoas[idPessoa].cpf);
    if (mostrarDispositivo == 1 && pessoas[idPessoa].dispositivo > 0)
    {
      for (idDispositivo = 0; idDispositivo < pessoas[idPessoa].quantDispositivos; idDispositivo++)
      {
        printf("\tDescricao: %s\n", pessoas[idPessoa].dispositivo[idDispositivo].descricao);
        printf("\tValor: %d\n", pessoas[idPessoa].dispositivo[idDispositivo].valor);
        if (idDispositivo + 1 != pessoas[idPessoa].quantDispositivos)
          printf("\n");
      }
    }
  }
}

struct Pessoa *cadastrarDispositivo(struct Pessoa *pessoas, int *tamanho)
{
  int idPessoa;
  printf("Digite o indice da pessoa: ");
  scanf("%d", &idPessoa);
  if (idPessoa > 0 && idPessoa <= *tamanho)
  {
    idPessoa -= 1;
    if (pessoas[idPessoa].quantDispositivos == 0)
    {
      pessoas[idPessoa].dispositivo = (struct Dispositivo *)calloc(pessoas[idPessoa].quantDispositivos + 1, sizeof(struct Dispositivo));
    }
    if (pessoas[idPessoa].quantDispositivos > 0)
    {
      pessoas[idPessoa].dispositivo = (struct Dispositivo *)realloc(pessoas[idPessoa].dispositivo, (pessoas[idPessoa].quantDispositivos + 1) * sizeof(struct Dispositivo));
    }
    printf("Valor: ");
    scanf("%d", &pessoas[idPessoa].dispositivo[pessoas[idPessoa].quantDispositivos].valor);
    printf("Descricao: ");
    pessoas[idPessoa].dispositivo[pessoas[idPessoa].quantDispositivos].descricao = alocarChar(100);
    scanf("%s", pessoas[idPessoa].dispositivo[pessoas[idPessoa].quantDispositivos].descricao);
    pessoas[idPessoa].quantDispositivos += 1;
  }
  else{
    printf("Indice invalido");
  }
  return pessoas;
}

struct Pessoa *cadastrarPessoa(struct Pessoa *pessoas, int *tamanho)
{
  if (*tamanho == 0)
    pessoas = (struct Pessoa *)calloc(*tamanho + 1, sizeof(struct Pessoa));
  else
    pessoas = (struct Pessoa *)realloc(pessoas, (*tamanho + 1) * sizeof(struct Pessoa));
  printf("Nome: ");
  pessoas[*tamanho].nome = alocarChar(100);
  scanf("%s", pessoas[*tamanho].nome);
  printf("CPF: ");
  scanf("%d", &pessoas[*tamanho].cpf);
  pessoas[*tamanho].quantDispositivos = 0;
  *tamanho += 1;
  return pessoas;
}

struct Pessoa *removerPessoa(struct Pessoa *pessoas, int *tamanho)
{
  int idPessoa;
  printf("Digite o indice da pessoa: ");
  scanf("%d", &idPessoa);
  if (idPessoa > 0 && idPessoa <= *tamanho)
  {
    idPessoa -= 1;
    for (int i = idPessoa; i < *tamanho - 1; i++)
    {
      pessoas[i] = pessoas[i + 1];
    }
    *tamanho -= 1;
    pessoas = (struct Pessoa *)realloc(pessoas, (*tamanho) * sizeof(struct Pessoa));
    printf("Pessoa deletada\n");
  }
  else
  {
    printf("Indice da pessoa invalido\n");
  }
  return pessoas;
}

struct Pessoa *removerDispositivo(struct Pessoa *pessoas, int *tamanho)
{
  int idPessoa, indexDispositivo, i;
  if (*tamanho > 0)
  {
    mostrarPessoasDispositivos(pessoas, *tamanho, 0);
    printf("Digite o indice da pessoa: ");
    scanf("%d", &idPessoa);
    if (idPessoa > 0 && idPessoa <= *tamanho)
    {
      idPessoa -= 1;
      if (pessoas[idPessoa].quantDispositivos > 0)
      {
        for (i = 0; i < pessoas[idPessoa].quantDispositivos; i++)
        {
          printf("%d - Descricao: %s\nValor: %s\n", i + 1, pessoas[idPessoa].dispositivo[i].descricao, pessoas[idPessoa].dispositivo[i].descricao);
          if (i + 1 != pessoas[idPessoa].quantDispositivos)
          {
            printf("\n");
          }
        }
        printf("Digite o indice do dispositivo: ");
        scanf("%d", &indexDispositivo);
        if (indexDispositivo > 0 && indexDispositivo <= pessoas[idPessoa].quantDispositivos)
        {
          indexDispositivo -= 1;
          pessoas[idPessoa].quantDispositivos -= 1;
          for (i = indexDispositivo; i < pessoas[idPessoa].quantDispositivos; i++)
          {
            pessoas[idPessoa].dispositivo[i] = pessoas[idPessoa].dispositivo[i + 1];
          }

          pessoas[idPessoa].dispositivo = (struct Dispositivo *)realloc(pessoas[idPessoa].dispositivo, (pessoas[idPessoa].quantDispositivos) * sizeof(struct Dispositivo));
        }
        else
        {
          printf("Indice do dispositivo invalido\n");
        }
      }
      else
      {
        printf("Nenhum dispositivo cadastrado\n");
      }
    }
    else
    {
      printf("Indice da pessoa invalido\n");
    }
  }
  return pessoas;
}

void main()
{
  struct Pessoa *pessoa = NULL;
  int tamanhoPessoa = 0;

  do
  {
    switch (menu())
    {
    case 1:
      pessoa = cadastrarPessoa(pessoa, &tamanhoPessoa);
      break;
    case 2:
      pessoa = cadastrarDispositivo(pessoa, &tamanhoPessoa);
      break;
    case 3:
      mostrarPessoasDispositivos(pessoa, tamanhoPessoa, 1);
      break;
    case 4:
      pessoa = removerPessoa(pessoa, &tamanhoPessoa);
      break;
    case 5:
      pessoa = removerDispositivo(pessoa, &tamanhoPessoa);
      break;
    case 0:
      free(pessoa);
      exit(0);
      break;
    default:
      printf("Opcao invalida\n");
    }
  } while (1);
}