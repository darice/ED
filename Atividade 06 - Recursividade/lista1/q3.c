// Crie uma função recursiva que imprima em ordem inversa os dígitos de um valor inteiro(de
// tamanho qualquer) inserido pelo usuário.

#include <stdio.h>

int inverter(int n, int novoN){
    int resto = n % 10;
    n = n/10;
    novoN = novoN*10 + resto;
    if(n == 0){
        return novoN;
    }
    return inverter(n, novoN);
}

void main(){
    int n;
    printf("Digite o número:");
    scanf("%d",&n);
    printf("%d", inverter(n, 0));
}