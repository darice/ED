// Utilizando alocação dinâmica, refaça o exercício abaixo(da lista anterior),
// acrescentando as funções para alocar e liberar espaço de memória às estruturas
// manipuladas (Ou seja, a quantidade de jogadores e reservas deve ser apresentada
// em tempo de execução).

// Crie uma struct para armazenar os dados (nome, idade, posição,numero_camisa) dos jogadores de vôlei de um time. Crie duas estruturas
// para armazenar os jogadores principais e os reservas. Desenvolva um
// programa que o usuário consiga entrar com os jogadores principais(seis)
// e reservas(4) e mostre-os na tela. Para Facilitar a entrada de dados,
// utilize um menu.

#include <stdio.h>
#include <stdlib.h>

struct jogador
{
    char *nome;
    int idade;
    char *posicao;
    int numeroCamisa;
};

typedef struct jogador reservas;

typedef struct jogador principais;

int menu()
{
    int op;
    printf("---------MENU----------\n");
    printf("0 - Sair\n");
    printf("1 - Cadastrar jogadores principais\n");
    printf("2 - Cadastrar jogadores reservas\n");
    printf("3 - Mostrar jogadores\n");
    scanf("%d", &op);
    return op;
}

char *alocarChar(int t)
{
    char *string = (char *)malloc(sizeof(char) * t);
    return string;
}

struct jogador *cadastrarJogador(struct jogador *j, int *quantidade)
{
    struct jogador *jogadoresNovo = (struct jogador *)realloc(j, sizeof(struct jogador) * (1 + (*quantidade)));
    printf("Nome ");
    jogadoresNovo[*quantidade].nome = alocarChar(100);
    scanf("%s", jogadoresNovo[*quantidade].nome);
    printf("Idade ");
    scanf("%d", &jogadoresNovo[*quantidade].idade);
    printf("Posicao ");
    jogadoresNovo[*quantidade].posicao = alocarChar(100);
    scanf("%s", jogadoresNovo[*quantidade].posicao);
    printf("Numero da camisa ");
    scanf("%d", &jogadoresNovo[*quantidade].numeroCamisa);
    (*quantidade)++;
    return jogadoresNovo;
}

void mostrarJogadores(struct jogador *reservas, int quantReservas, struct jogador *principais, int quantPrincipais)
{
    printf("Principais:\n");
    for (int i = 0; i < quantPrincipais; i++)
    {
        printf("Nome: %s\n", principais[i].nome);
        printf("Idade: %d\n", principais[i].idade);
        printf("Posição: %s\n", principais[i].posicao);
        printf("Número da camisa: %d\n\n", principais[i].numeroCamisa);
    }

    printf("\nReservas:\n");
    for (int i = 0; i < quantReservas; i++)
    {
        printf("Nome: %s\n", reservas[i].nome);
        printf("Idade: %d\n", reservas[i].idade);
        printf("Posição: %s\n", reservas[i].posicao);
        printf("Número da camisa: %d\n\n", reservas[i].numeroCamisa);
    }
}

void main()
{
    struct jogador *reservas = NULL;
    struct jogador *principais = NULL;

    int quantPrincipais = 0;
    int quantReservas = 0;
    int op = menu();

    while (1)
    {
        switch (op)
        {
        case 1:
            principais = cadastrarJogador(principais, &quantPrincipais);
            break;
        case 2:
            reservas = cadastrarJogador(reservas, &quantReservas);
            break;
        case 3:
            mostrarJogadores(reservas, quantReservas, principais, quantPrincipais);
            break;
        case 0:
            free(reservas);
            free(principais);
            exit(0);
            break;
        default:
            printf("Valor invalido \n");
            break;
        }
        op = menu();
    };
}