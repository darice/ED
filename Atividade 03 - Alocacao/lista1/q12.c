// Crie um programa para manipular vetores. O seu programa deve implementar uma
// função que receba um vetor de inteiros V e retorne um outro vetor de inteiros alocado
// dinamicamente com todos os valores de V que estejam entre o valor mínimo e máximo
// (que também são passados como parâmetro para a função).

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int *criarVetor(int n)
{
    int *pont;
    pont = (int *)calloc(n, sizeof(int));
    return pont;
}

void imprimirVetor(int *vetor, int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("%d, ", vetor[i]);
    }
}

void liberarVetor(int *vetor)
{
    free(vetor);
}

int *novoVetor(int *vetor, int max, int min)
{
    int *pont = (int *)calloc(max-min+1, sizeof(int));
    int j=min;
    for (int i = 0; i <= max-min+1; i++)
    {
       pont[i] = j;
       j++;
    }

    return pont;
}

int main()
{
    int n = 3;
    int *vetor = criarVetor(3);
    vetor[0] = 3;
    vetor[1] = 1;
    vetor[2] = 10;
    int max = 10;
    int min = 1;
    int *outroVetor = novoVetor(outroVetor, max, min);
    imprimirVetor(outroVetor, max-min+1);
    liberarVetor(vetor);
    liberarVetor(outroVetor);
}
