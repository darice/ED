#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float calcDistancia(int x1, int y1, int z1, int x2, int y2, int z2)
{
    return (float)sqrt(pow(x2 - x1, 2.0) + pow(y2 - y1, 2.0)+pow(z2 - z1, 2.0));
}


void main()
{
    int ***cubo;

    int lado;
    printf("Digite a medida do lado do cubo:");
    scanf("%d", &lado);
    int raio = (int)lado / 2;
    cubo = (int ***)calloc(lado, sizeof(int **));
    for (int i = 0; i < lado; i++)
    {
        cubo[i] = (int **)calloc(lado, sizeof(int *));
    }
    for (int i = 0; i < lado; i++)
    {
        for (int j = 0; j < lado; j++)
        {
            cubo[i][j] = (int *)calloc(lado, sizeof(int));
        }
    }

    for (int i = 0; i < lado; i++)
    {
        for (int j = 0; j < lado; j++)
        {
            for (int k = 0; k < lado; k++)
        {
            int distancia = (int) calcDistancia(raio, raio, raio, i, j, k);
            if(distancia <= raio){
                cubo[i][j][k] = 1;
            }
        }
        }
    }

    for (int i = 0; i < lado; i++)
    {
        for (int j = 0; j < lado; j++)
        {
            for (int k = 0; k < lado; k++){
            printf("%d ", cubo[i][j][k]);

            }
            printf("\n");
        }
        printf("\n");
    }
}



