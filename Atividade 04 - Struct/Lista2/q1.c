// Fazer um registro (struct) para um cliente, com: nome, RG, CPF, idade, endereço
// residencial, endereço de trabalho, telefone celular, telefone fixo. Na função principal
// solicite ao usuário que preencha essa estrutura, depois imprima os dados que foram
// cadastrados.

#include <stdio.h>
#include <stdlib.h>

struct Cliente
{
    char nome[100];
    int rg;
    int cpf;
    int idade;
    char enderecoResidencial[200];
    char enderecoTrabalho[200];
    int celular;
    int telefone;
};

typedef struct Cliente Cliente;

void main()
{
    Cliente cliente;
    printf("Digite o nome do cliente: ");
    scanf("%s", cliente.nome);
    printf("Digite o Rg do cliente: ");
    scanf("%d", &cliente.rg);
    printf("Digite o CPF do cliente: ");
    scanf("%d", &cliente.cpf);
    printf("Digite a idade do cliente: ");
    scanf("%d", &cliente.idade);
    printf("Digite o endereco residencial do cliente: ");
    setbuf(stdin, NULL);
    scanf("%[^\n]s", cliente.enderecoResidencial);
    printf("Digite o endereco comercial do cliente: ");
    setbuf(stdin, NULL);
    scanf("%[^\n]s", cliente.enderecoTrabalho);
    printf("Digite o telefone celular do cliente: ");
    scanf("%d", &cliente.celular);
    printf("Digite o telefone fixo do cliente: ");
    scanf("%d", &cliente.telefone);

    printf("\n");
    printf("Nome do cliente: %s\n", cliente.nome);
    printf("Rg do cliente: %d\n", cliente.rg);
    printf("CPF do cliente: %d\n", cliente.cpf);
    printf("Idade do cliente: %d\n", cliente.idade);
    printf("Endereco residencial do cliente: %s\n", cliente.enderecoResidencial);
    printf("Endereco comercial do cliente: %s\n",cliente.enderecoTrabalho);
    printf("Telefone celular do cliente: %d\n",cliente.celular);
    printf("Telefone fixo do cliente: %d\n",cliente.telefone);
}