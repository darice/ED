// Faça um programa que leia o tamanho de um vetor de inteiros e reserve
// dinamicamente memoria para esse vetor. Em seguida, leia os elementos desse vetor,
// imprima o vetor lido e mostre o resultado da soma dos numeros ímpares presentes no
// vetor.

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int * criarVetor(int n){
    int * pont;
    pont = (int *) calloc(n, sizeof(int));
    return pont;
}

void imprimirVetor(int * vetor, int n){
    for(int i=0; i<n; i++){
        printf("%d, ", vetor[i]);
    }
}

void liberarVetor(int *vetor){
    free(vetor);
}

void somaImpares(int *vetor, int n){
    int soma = 0;
    for(int i=0; i<n; i++){
        if(vetor[i] % 2 == 1){
            soma += vetor[i];
        }
    }
    printf("Soma dos elementos impares: %d", soma);
}

int main(){
    int n;
    scanf("%d", &n);
    int * vetor = criarVetor(n);
    srand(time(NULL));
    for(int i=0; i<n; i++){
        vetor[i] = rand() % 10;
    }
    imprimirVetor(vetor, n);
    somaImpares(vetor, n);
    liberarVetor(vetor);
}