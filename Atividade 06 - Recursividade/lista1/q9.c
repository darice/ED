// 9)Crie uma funcao que calcule o fatorial quádruplo de um número N é dado por
// (2*N)!/N!

#include <stdio.h>

int fatorialQuadruplo(int n, int x){
  if (n == x)
    return 1;
  return x * fatorialQuadruplo(n, x-1);
}

void main(){
    int n = 3;
    printf("%d", fatorialQuadruplo(n, 2*n));
}