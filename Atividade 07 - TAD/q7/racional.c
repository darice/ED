#include <stdio.h>
#include <stdlib.h>
#include "racional.h"

struct racional
{
    int numerador;
    int denominador;
};

Racional *criarRacional(int numerador, int denominador)
{
    Racional *racional;
    racional = (Racional *)calloc(1, sizeof(Racional));
    racional->numerador = numerador;
    racional->denominador = denominador;
    return racional;
}

Racional *somar(Racional *racional1, Racional *racional2)
{
    Racional *soma;
    soma->numerador = racional1->numerador * racional2->denominador + racional2->numerador * racional1->denominador;
    soma->denominador = racional1->denominador * racional2->denominador;
    return soma;
}

Racional *multiplicar(Racional *racional1, Racional *racional2)
{
    Racional *produto;
    produto->numerador = racional1->numerador * racional2->numerador;
    produto->denominador = racional1->denominador * racional2->denominador;
    return produto;
}

void *verifica(Racional *racional1, Racional *racional2)
{
    int numerador1 = racional1->numerador * racional2->denominador;
    int numerador2 = racional2->numerador * racional1->denominador;
    if (numerador1 == numerador2)
    {
        printf("São iguais");
    }
    else
    {
        printf("Não são iguais");
    }
}

void imprimirRacional(Racional *racional)
{
    printf("%d/%d\n", racional->numerador, racional->denominador);
}

