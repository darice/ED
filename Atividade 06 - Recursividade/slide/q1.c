// Crie uma função recursiva que calcule a exponenciação de
// um valor b por um expoente p sem usar o operador de
// exponenciação.

#include <stdio.h>

int expo(int b, int p){
    if(p == 1){
        return b;
    }
    if(p == 0){
        return 1;
    }
    return b * expo(b, p-1);
}

void main(){
    printf("%d", expo(3,3));
}