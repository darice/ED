#include <stdio.h>
#include <stdlib.h>

struct no
{
    int info;
    struct no *prox;
};
typedef struct no No;

No *criarLista()
{
    return NULL;
}

No *inserirElementoInicio(No *lista, int info)
{
    No *novo;
    novo = (No *)malloc(sizeof(No));
    novo->info = info;
    novo->prox = lista;

    return novo;
}

void mostrarLista(No *lista)
{
    No *aux;
    aux = lista;
    if (aux == NULL)
        printf("lista vazia\n");
    else
    {
        while (aux != NULL)
        {
            printf("%d ", aux->info);
            aux = aux->prox;
        }
    }
}

No* removeElemento(No *lista, int valor)
{
    No *aux, *anterior = NULL;
    aux = lista;
    while (aux != NULL && aux->info != valor)
    {
        anterior = aux;
        aux = aux->prox;
    }
    if (aux == NULL)
        printf("Número não encontrado na lista!\n");
    else
    {
        if (anterior == NULL)
            lista = aux->prox;
        else
            anterior->prox = aux->prox;
    }
    free(aux);
    return lista;
}

void liberaLista(No *lista){
    No *aux;
    aux = lista;
    while(aux->prox!=NULL){
        No*t;
        t = aux-> prox;
        free(aux);
        aux = t;
    }
}

No* inserirElementoFinal(No*lista, int info){
    No* novo;
    novo = (No*) malloc(sizeof(No));
    novo-> info = info;
    if(lista == NULL){
        novo -> prox = lista;
        lista = novo;
    }
    else{
        No*aux;
        aux = lista;
        while(aux->prox != NULL){
            aux = aux->prox;
        }
        novo-> prox = aux-> prox;
        aux-> prox = novo;
    }
    return lista;
}

void main()
{
    No*lista;
    lista = criarLista();
    lista = inserirElementoInicio(lista, 23);
    lista = inserirElementoInicio(lista, 11);
    lista = inserirElementoFinal(lista, 7);
    mostrarLista(lista);
    lista = removeElemento(lista, 7);
    mostrarLista(lista);
}
