#include "biblioteca.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct livro
{
    char titulo[50];
    char autor[30];
    char genero[10];
    int ano;
};


Livro *livroCria(char titulo[], char autor[], char genero[], int ano)
{
    Livro *livro = (Livro *)calloc(1, sizeof(Livro));
    strcpy(livro->titulo, titulo);
    strcpy(livro->autor, autor);
    strcpy(livro->genero, genero);
    livro->ano = ano;
    return livro;
}

char *livroObtemGenero(Livro *livro)
{
    return livro->genero;
}

char *livroObtemAutor(Livro *livro)
{
    return livro->autor;
}

char *livroObtemTitulo(Livro *livro)
{
    return livro->titulo;
}

int livroObtemAno(Livro *livro)
{
    return livro->ano;
}

int verifica(Livro *livro)
{
    if (livro->ano < 1930)
        return -1;
    else if (livro->ano <= 1945)
        return 0;
    else
        return 1;
    
}
