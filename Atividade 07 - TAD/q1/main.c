#include <stdio.h>
#include "calculos.h"

void menu(int *op)
{
    do
    {
        printf("\nEscolha uma opção:\n");
        printf("1-Somar;\n2-Subtrair;\n3-Dividir;\n4-Multiplicar;\n5-Sair.\n");
        scanf("%d", &(*op));
    } while ((*op) < 1 || (*op) > 5);
}

void numeros(float *p_num1, float *p_num2)
{
    printf("\nDigite dois valores:\n");
    scanf("%f", &(*p_num1));
    scanf("%f", &(*p_num2));
}

void main()
{
    int op;
    float num1, num2, resultado;
    menu(&op);
    do
    {
        numeros(&num1, &num2);
        switch (op)
        {
        case 1:
            soma(num1, num2, &resultado);
            break;
        case 2:
            subtrair(num1, num2, &resultado);
            break;
        case 3:
            dividir(num1, num2, &resultado);
            break;
        case 4:
            multiplicar(num1, num2, &resultado);
            break;
        }
        printf("\nResultado: %f\n", resultado);
        menu(&op);
    } while (op != 5);
}