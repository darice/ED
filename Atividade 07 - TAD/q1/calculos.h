#ifndef calculos_h
#define calculos_h

#include<stdio.h>

void soma(float num1, float num2, float* p_result);
void subtrair(float num1, float num2, float* p_result);
void dividir(float num1, float num2, float* p_result);
void multiplicar(float num1, float num2, float* p_result); 

#endif