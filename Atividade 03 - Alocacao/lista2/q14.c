// Construa um programa que aloque em uma matriz, de maneira dinâmica, os itinerários, horários
// e respectivos preços de passagens de uma determinada companhia de transportes. De modo que o
// operador de transportes possa, em tempo de execução, definir a quantidade de cidades que sua
// empresa trabalha. Escreva as funções que julgar necessária e realize testes no método main.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char **adicionarCidade(char **mat, int *tamanho)
{
    if (*tamanho == 0)
    {
        *tamanho = 1; 
    }
    else
    {
        (*tamanho)++;
        mat = (char **)realloc(mat, (*tamanho) * sizeof(char *));
    }

    char itinerario[100], horario[10], preco[10];

    printf("Cidade: \n");

    printf("Itinerario: ");
    setbuf(stdin, NULL);
    scanf("%[^\n]s", itinerario);
    setbuf(stdin, NULL);

    printf("Horario: ");
    scanf("%s", horario);

    printf("Preco: R$ ");
    scanf("%s", preco);

    int len = strlen(itinerario);
    len += strlen(horario);
    len += strlen(preco);

    char *stringFinal = (char *)calloc(len+6, sizeof(char));
    mat[*tamanho-1] = (char *)calloc(len+6, sizeof(char));

    strcat(stringFinal, itinerario);
    strcat(stringFinal, " - ");
    strcat(stringFinal, horario);
    strcat(stringFinal, " R$");
    strcat(stringFinal, preco);

    strcpy(mat[*tamanho - 1], stringFinal);
    return mat;
}

void imprimir(char **mat, int tamanho)
{
    printf("%d", tamanho);
    for (int i = 0; i < tamanho; i++)
    {
        printf("%s\n", mat[i]);
    }
}

int menu()
{
    int op;
    printf("\n\n---------MENU----------\n");
    printf("0 - Sair\n");
    printf("1 - Cadastrar intinerários\n");
    printf("2 - Mostrar itinerários cadastrados\n");
    scanf("%d", &op);
    return op;
}

void main()
{
    char **mat = (char **)calloc(1, sizeof(char *));
    int tamanho = 0;

    int op = menu();

    while (1)
    {
        switch (op)
        {
        case 1:
            mat = adicionarCidade(mat, &tamanho);
            break;
        case 2:
            imprimir(mat, tamanho);
            break;
        default:
            printf("Valor invalido \n");
            break;
        }
        op = menu();
    };
}
