// Escreva uma função recursiva que calcule o produto de todos os números que estão
// presentes em uma posição par de um vetor de inteiros.

#include <stdio.h>

int produtoArrayPosicaoPar(int vet[], int tamanho){
    tamanho -= 1;
    if(tamanho == 0){
        return vet[tamanho];
    }
    if(tamanho % 2 == 0)
        return vet[tamanho]* produtoArrayPosicaoPar(vet, tamanho);
    return produtoArrayPosicaoPar(vet, tamanho);
}

void main(){
    int vetor[] = {4,3,10, 7, 2};
    printf("%d\n", produtoArrayPosicaoPar(vetor, 5));
}