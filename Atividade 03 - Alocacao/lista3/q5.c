// Crie um algoritmo que, após receber do usuário uma cadeia simples de
// caracteres (apenas permitindo letras), seja capaz de imprimir na tela a mesma
// sequência inserida pelo usuário em letras maiúsculas, minúsculas e preencha um
// vetor com a sequência de caracteres inseridos alternando entre maiúsculas e
// minúsculas.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void imprimirMaiscula(char *string)
{
    for (int i = 0; string[i] != '\0'; i++)
    {
        printf("%c", toupper(string[i]));
    }
}

void imprimirMinuscula(char *string)
{
    for (int i = 0; string[i] != '\0'; i++)
    {
        printf("%c", tolower(string[i]));
    }
}

void imprimirAlternada(char *string)
{
    char *string2 = (char *)calloc(strlen(string), sizeof(char *));
    for (int i = 0; string[i] != '\0'; i++)
    {
        if (i % 2 == 0)
        {
            string2[i] = tolower(string[i]);
        }
        else
        {
            string2[i] = toupper(string[i]);
        }
    }
    for (int i = 0; string2[i] != '\0'; i++)
    {
        printf("%c", string2[i]);
    }
}

void main()
{
    char *string = (char *)calloc(100, sizeof(char *));
    int erro;
    do
    {
        erro = 0;
        printf("Digite uma palavras (apenas letras): ");
        scanf("%s", string);
        for (int i = 0; string[i] != '\0'; i++)
        {
            if (!isalpha(string[i]))
            {
                erro = 1;
                break;
            }
        }
    } while (erro == 1);

    imprimirMaiscula(string);
    printf("\n");
    imprimirMinuscula(string);
    printf("\n");
    imprimirAlternada(string);
}