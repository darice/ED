// Escreva uma função recursiva maxmin que calcule o valor de um elemento máximo
// e o valor de um elemento mínimo de um vetor.

#include <stdio.h>

void maxmin(int vet[], int tamanho, int max, int min)
{
    tamanho -= 1;
    if (tamanho == 0)
    {
        printf("max: %d\nmin: %d\n", max, min);
        return;
    }
    if (vet[tamanho] < min)
    {
        min = vet[tamanho];
    }
    if (vet[tamanho] > max)
    {
        max = vet[tamanho];
    }
    return maxmin(vet, tamanho, max, min);
}

void main()
{
    int tamanho = 5;
    int vetor[] = {4, 3, 7, 2, 10};
    maxmin(vetor, tamanho, vetor[0], vetor[0]);
}